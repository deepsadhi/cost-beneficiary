<?php

defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

require_once('.'.DS.'connect.php');
require_once('.'.DS.'htmlpurifier'.DS.'HTMLPurifier.standalone.php');


class Data
{
    private $dbh = null;
    private $get = array();
    private $data = array();


    public function purify(array $get){
        $config = HTMLPurifier_Config::createDefault();
        $purifier = new HTMLPurifier($config);

        foreach($get as $key => $value){
            $key = $purifier->purify($key);
            $value = $purifier->purify($value);
            $this->get[$key] = $value;
        }
    }

    private function connectDB(){
        global $DB;
        try{
            $this->dbh = new PDO('mysql:host='.$DB['host'].';dbname='.$DB['name'], $DB['user'], $DB['pass']);
        } catch(PDOException $e){
            die('Error!');
        }
    }

    private function query($sql){
        $this->connectDB();

        try{
            $stmt = $this->dbh->prepare($sql);
            $stmt->execute();
            $this->data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt->closeCursor();
            $this->dbh = null;
        } catch(PDOException $e){
            die('Error!');
        }
    }

    private function checkGET(array $get){
        foreach($get as $g){
            if(!array_key_exists($g, $this->get)){
                die('Hack attempt?');
            }
        }
    }

    private function removeKeys(){
        $data = array();
        foreach($this->data as $d){
            $key = array_keys($d);
            $key = array_shift($key);
            array_push($data, $d[$key]);
        }
        $this->data = $data;
    }  

    public function prepData(){
        $this->checkGET(array('data'));
        $get = $this->get['data'];

        switch (strtolower($get)){
            case "details":
                $this->checkGET(array('projectid'));
                $projectID = $this->get['projectid'];
                $sql = 'SELECT *, project_cost / beneficiary AS cost_per_beneficiary  FROM projects WHERE id="'.$projectID.'"';
                $this->query($sql);
		$this->opJSON();
                break;

            case "donors":
                $sql = 'SELECT donor AS label, AVG(project_cost / beneficiary) AS amount FROM projects GROUP BY donor ORDER BY amount ASC';
                $this->query($sql);
                $this->opTSV();
                break;

            case "sectors":
                $sql = 'SELECT sector AS label, AVG(project_cost / beneficiary) AS amount FROM projects GROUP BY sector ORDER BY amount ASC';
                $this->query($sql);
                $this->opTSV();
                break;

            case "projects":
                $sql = 'SELECT project_name AS label, project_cost / beneficiary AS amount FROM projects ORDER BY amount ASC';
                $this->query($sql);
                $this->opTSV();
                break;

            case "projectlist":
                $sql = 'SELECT CONCAT_WS(":=",id, project_name) FROM projects ORDER BY project_name';
                $this->query($sql);
                $this->removeKeys();
		$this->opJSON();
                break;

            default:
                echo 'Hack attempt?';
                break;
        }
    }

    private function opJSON(){
	    echo json_encode($this->data);
    }

    private function opTSV(){
        echo "label\tamount\n";        
        foreach($this->data as $d){
            echo $d['label']."\t".$d['amount']."\n";
        } 
    }

}


$get = $_GET;

$data = new Data;
$data->purify($get);
$data->prepData();
