-- phpMyAdmin SQL Dump
-- version 4.2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2014 at 11:53 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cost_ben`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
`id` int(11) NOT NULL,
  `donor` varchar(50) NOT NULL,
  `sector` varchar(50) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `project_cost` int(11) NOT NULL,
  `beneficiary` bigint(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `donor`, `sector`, `project_name`, `location`, `start_date`, `end_date`, `project_cost`, `beneficiary`) VALUES
(1, 'World Bank', 'Povery Alleviation', 'Poverty Alleviation Fund Project', 'Nepal', '2004-08-30', '2009-02-09', 16600000, 194560),
(2, 'World Bank', 'Health', 'Nepal Health Sector Program Project', 'Nepal', '2005-02-25', '2010-07-15', 498280000, 31967973),
(3, 'World Bank', 'Energy', 'GPOBA - Nepal Biodigesters', 'Nepal', '2007-07-10', '2013-04-30', 5000000, 27139),
(4, 'World Bank', 'Water', 'Second Rural Water Supply and Sanitation Project', 'Nepal', '2005-01-17', '2012-08-31', 41500000, 1140892),
(5, 'World Bank', 'Povery Alleviation', 'Nepal Poverty Alleviation Fund - additional Financ', '', '2006-09-12', '2009-02-01', 25000000, 314000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
 ADD PRIMARY KEY (`id`), ADD KEY `donor` (`donor`,`project_name`,`location`), ADD KEY `sector` (`sector`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
