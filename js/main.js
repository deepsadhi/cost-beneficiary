function spin(t){
	var opts = {
		lines: 9, // The number of lines to draw
		length: 0, // The length of each line
		width: 10, // The line thickness
		radius: 10, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 11, // The rotation offset
		direction: 1, // 1: clockwise, -1: counterclockwise
		color: '#1f8841', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 61, // Afterglow percentage
		shadow: false, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		//zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: 'auto', // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	};
	var target = document.getElementById(t);
	var spinner = new Spinner(opts).spin(target);
}

var plot = (function (target) {
    spin('bar_'+target);
    $.ajax({    
        url: 'data.php?data='+target,
        type: "GET"
    })
    .done(function(data){
        $('#tsv_'+target).text(data);
        plot();
    })
    .fail(function(jqXHR, textStatus){
        $('#bar_'+target).append("Could not compelete your request :( " + textStatus);
    });


    function plot(){
    Highcharts.data({
        csv: document.getElementById('tsv_'+target).innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];
            
            // Parse percentage strings
            columns[1] = $.map(columns[1], function (value) {
                    value = parseFloat(value);
                return value;
            });

            $.each(columns[0], function (i, name) {
                var brand,
                    version;

                if (i > 0) {

                    // Remove special edition notes
                    name = name.split(' -')[0];

                    // Split into brand and version
                    version = name.match(/([0-9]+[\.0-9x]*)/);
                    if (version) {
                        version = version[0];
                    }
                    brand = name.replace(version, '');

                    // Create the main data
                    if (!brands[brand]) {
                        brands[brand] = columns[1][i];
                    } else {
                        brands[brand] += columns[1][i];
                    }

                    // Create the version data
                    if (version !== null) {
                        if (!versions[brand]) {
                            versions[brand] = [];
                        }
                        versions[brand].push(['v' + version, columns[1][i]]);
                    }
                }
                
            });

            $.each(brands, function (name, y) {
                brandsData.push({ 
                    name: name, 
                    y: y,
                    drilldown: versions[name] ? name : null
                });
            });
            $.each(versions, function (key, value) {
                drilldownSeries.push({
                    name: key,
                    id: key,
                    data: value
                });
            });

            // Create the chart
            $('#bar_'+target).highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    type: 'category'
                },
                yAxis: {
                    title: {
                        text: 'Cost per Beneficiary'
                    },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '${point.y:.1f}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>${point.y:.2f}</b><br/>'
                }, 

                series: [{
                    name: 'Cost per Beneficiary',
                    colorByPoint: true,
                    data: brandsData
                }],
                drilldown: {
                    series: drilldownSeries
                },
	  
		//colors: ['#000000', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
        //colors: ['#1F3C29', '#1F8841', '#2ABB5A', '#27AE53', '#71C78D', '#64E572', '#FF9655', '#FFF263', '#6AF9C4'] 
        colors:['#ccddcc']
            })

        }
    });
    }
});


function loadOptions(url, inpSelect){
    var url = 'data.php?data='+url;
    $.ajax({    
        url: url,
        type: "GET",
        data: "json"
    })
    .done(function(data){
        data = JSON.parse(data);
        addOptions(inpSelect, '', data);
    })
    .fail(function(jqXHR, textStatus){
        alert("Could not compelete your request :( " + textStatus)
    });
}

function addOptions(id, select, options){
    $.each(options, function( index, value){
        if(id != 'projects'){
            $('#'+id)
                .append($("<option></option>")
                .attr('value',value)
                .text(value));
        }else{
            var res = value.split(':=');
            $('#'+id)
                .append($("<option></option>")
                .attr('value',res[0])
                .text(res[1]));
        }
    });

}

function loadProject(url){
    var url = 'data.php?data=details&'+url;
    $.ajax({    
        url: url,
        type: "GET",
        data: "json"
    })
    .done(function(data){
        data = JSON.parse(data);
        data = data[0];
        for(d in data){
            $('#'+d).text(data[d]);
        }
    })
    .fail(function(jqXHR, textStatus){
        alert("Could not compelete your request :( " + textStatus)
    });
}

