Cost-Beneficiary
================

The app aims to analyze cost efficiency of donor funded projects in Nepal.


VERSION:
--------
1.0.0


CONTRIBUTORS:
-------------
* [Ashmita](http://about.me/mishraashmita)
* [Ayush](http://about.me/ayushmaharjan)
* [Deepak](http://about.me/deepsadhi)
* [Kshitiz](http://about.me/kshitiztiwari)
* [Manisha](http://about.me/manisapanta)
* [Sujata](http://about.me/sujata.shrestha)

CONTACT US:
-----------
[mail@bctians.com](mail@bctians.com)
